""" A simple social network. """

class Person:
    """ A person in the social network.
    
    Attributes:
        name (str): the person's name.
        connections (set of Person): the person's connections.
    """
    
    def __init__(self, name):
        self.name = name
        self.connections = set()
    
    def add_connection(self, other):
        """ Add other as a connection.
        
        Args:
            other (Person): the person we are connecting with.
        
        Side effects:
            alters self.connections and other.connections.
        """
        if other not in self.connections:
            self.connections.add(other)
            other.add_connection(self)
        